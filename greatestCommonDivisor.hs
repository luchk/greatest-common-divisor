myGcd :: Integral a => a -> a -> a
myGcd 0 0 = error "Greatest Common Divisor feom 0 and 0 is not avalible"
myGcd x y = myGcd' (abs x) (abs y)
    where myGcd' x 0 = x
          myGcd' x y = myGcd' y (rem x y)