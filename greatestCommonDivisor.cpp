#include <iostream>
using namespace std;

int gcdRec(unsigned long int a, unsigned long int b)    //Варіант з рекурсією recursive option
{
	if (b == 0)
		return a;
	return gcdRec(b, a % b);
}

int gcdIteration(unsigned long int a, unsigned long int b)  // Метод ітерацій Iteration method
{
	int t;
	while (b != 0) {
		t = b;
		b = a % b;
		a = t;
	}
	return a;
}

int main()
{
	unsigned long int firstNumber;
	unsigned long int secondNumber;
	cout << "Ведіть перше число ";
	cin >> firstNumber;
	cout << "Ведіть друге число ";
	cin >> secondNumber;

	cout << "Рекурсивний варіант " << gcdRec(firstNumber , secondNumber) << endl;
	cout << "Ітераційний варіант " << gcdIteration(firstNumber , secondNumber) << endl;
	return 0;
}